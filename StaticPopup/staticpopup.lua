for i=1,STATICPOPUP_NUMDIALOGS do
	_G["StaticPopup"..i]:SetBackdropBorderColor(0,0,0,0)
	for j=1,4 do
		local button = _G["StaticPopup"..i.."Button"..j]
		button:SetNormalTexture([[Interface\DialogFrame\UI-DialogBox-Background-Dark]])
		button:SetHighlightTexture([[Interface\DialogFrame\UI-DialogBox-Gold-Background]])
		button:SetDisabledTexture("")
		button:SetPushedTexture([[Interface\DialogFrame\UI-DialogBox-Background-Dark]])
	end
end
