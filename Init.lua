local QUI = LibStub("AceAddon-3.0"):NewAddon("QUI","AceEvent-3.0","AceConsole-3.0","AceHook-3.0")

QUI.UIHider = CreateFrame("Frame")
QUI.UIHider:Hide()
QUI[1] = {}

function QUI:OnInitialize()
	self.db = LibStub("AceDB-3.0"):New("QUIDB",{profile = {}},true)
	--LibStub('LibDualSpec-1.0'):EnhanceDatabase(self.db, "QUI")
	self:RegisterChatCommand("QUI", "ChatCommand")

	self:RegisterEvent("ADDON_LOADED")
	local lods = {}
	local GetAddOnMetadata = GetAddOnMetadata
	for i=1,GetNumAddOns() do
		local conflict = GetAddOnMetadata(i,"X-QUI-CONFLICT")
		if conflict and not IsAddOnLoaded(conflict) then
			LoadAddOn(i)
		end
		local meta = GetAddOnMetadata(i,"X-QUI-LOD")
		if meta then
			local addon, title, notes, enabled, loadable, reason, security = GetAddOnInfo(i)
			if loadable == "DEMAND_LOADED" then
				lods[meta] = i
			end
		end
	end
	if next(lods) then
		QUI.lods = lods
		QUI:RegisterEvent("ADDON_LOADED")
	else
		QUI.ADDON_LOADED = nil
	end
	QUI.OnInitialize = nil
end

function QUI:ChatCommand(input)
--	LoadAddOn("QUI_O")
end

function QUI:ADDON_LOADED(event,addon,...)
	local lods = QUI.lods
	local index = QUI.lods[addon]
	if not index then return end
	LoadAddOn(index)
	lods[addon] = nil
	if next(lods) == nil then
		QUI:UnregisterEvent("ADDON_LOADED")
		QUI.ADDON_LOADED = nil
	end
end

function QUI.resume(current,...)
	local current_status = coroutine.status(current)
	if current_status =="suspended" then
		local status, msg = coroutine.resume(current,...)
		if not status then
			QUI:Print(msg)
		end
		return status,msg
	end
	return current_status
end

function QUI.skin_button_frame_template(frame)
	frame.Bg:SetTexture(131071)
	frame.Bg:SetAllPoints()
	frame.TitleBg:SetAlpha(0)
	frame.TopTileStreaks:SetAlpha(0)
	frame.Inset:SetAlpha(0)
	local name = frame:GetName()
	local _G = _G
	local scrollframe = _G[name.."ScrollFrame"]
	if scrollframe then
		scrollframe:SetAlpha(0)
	end
	local portraitframe = _G[name.."PortraitFrame"]
	if portraitframe then
		portraitframe:SetAlpha(0)
	end
	local icon = _G[name.."Icon"]
	if icon then
		icon:ClearAllPoints()
		icon:SetPoint("TOPLEFT")
	end
	local type = type
	for k,v in pairs(frame) do
		if type(k) == "string" and (k:find("Corner") or k:find("Border")) and type(v) == "table" then
			v:SetAlpha(0)
		end
	end
	_G[name.."BtnCornerLeft"]:SetAlpha(0)
	_G[name.."BtnCornerRight"]:SetAlpha(0)
	_G[name.."ButtonBottomBorder"]:SetAlpha(0)
end