local function skinframe(str)
	local frame = _G[str]
	local header = _G[str.."Header"]
	header:SetAlpha(0)
	local header_text = _G[str.."HeaderText"]
	header_text:ClearAllPoints()
	_G[str.."HeaderText"]:SetPoint("TOP",frame,"TOP",0,-15)
	frame:SetBackdropBorderColor(0,0,0,0)
end

skinframe("VideoOptionsFrame")
skinframe("InterfaceOptionsFrame")
